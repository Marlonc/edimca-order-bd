README DATABASE


THE FOLLOWING TXT FILE DETAILS THE SCRIPT TO RUN ON A POSTGRESQL VERSION 14.5 DATABASE FOR WINDOWS

STEP 1: YOU MUST HAVE INSTALLED POSTGRESQL VERSION 14.5 WITH PGADMIN 4
STEP 2: MAKE THE RESPECTIVE CONFIGURATIONS SUCH AS THE PASSWORD, THE PORT THE USER, MY CONFIGURATION IS AS FOLLOWS :
		username=postgres
		password=MarlonC1994
		port:5432
STEP 3: RIGHT CLICK IN DATABASES -> CREATE -> DATABASES...
STEP 4: PUT "edimcaProyecto" IN NAME OF DATABASE THEN CLICK IN SAVE
STEP 5: GO TO edimcaProyecto -> schemas -> public RIGHT CLICK CHOOSE QUERY TOOL
STEP 6: COPY FROM LINE 26 TO 177 AND PASTE IN QUERY AREA THEN CLICK IN EXECUTE
STEP 7: VALID IF EXECUTE IS SUCCESSFUL
STEP 8: RIGHT CLICK IN public AND CLICK IN REFRESH
STEP 9: YOU WILL BE ABLE TO OBSERVE THE TABLES AND SEQUENCES CREATED IN ADDITION TO THE USER FOR THE LOGIN








-----------------------------------------------------------------------------------------
--TABLES
-- DROP TABLE IF EXISTS public.users;

CREATE TABLE IF NOT EXISTS public.users
(
    usr_id bigint NOT NULL,
    usr_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    usr_last_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    usr_nick_name character varying(20) COLLATE pg_catalog."default" NOT NULL,
    usr_password character varying(20) COLLATE pg_catalog."default" NOT NULL,
    usr_create date NOT NULL,
    usr_last_update date,
    usr_state character varying(1) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (usr_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.users
    OWNER to postgres;


-- DROP TABLE IF EXISTS public.product;

CREATE TABLE IF NOT EXISTS public.product
(
    pdt_id bigint NOT NULL,
    pdt_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    pdt_price numeric(4,2) NOT NULL,
    pdt_create date NOT NULL,
    pdt_last_update date,
    pdt_state character varying(1) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT product_pkey PRIMARY KEY (pdt_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.product
    OWNER to postgres;



-- DROP TABLE IF EXISTS public.ordhead;

CREATE TABLE IF NOT EXISTS public.ordhead
(
    order_no bigint NOT NULL,
    cedula_cli character varying(20) COLLATE pg_catalog."default" NOT NULL,
    order_create date NOT NULL,
    order_price_total numeric DEFAULT 0,
    name_cli character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT ordhead_pkey PRIMARY KEY (order_no)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.ordhead
    OWNER to postgres;
	


-- DROP TABLE IF EXISTS public.orddetail;

CREATE TABLE IF NOT EXISTS public.orddetail
(
    order_no bigint NOT NULL,
    pdt_id bigint NOT NULL,
    quantity numeric NOT NULL,
    unit_cost numeric NOT NULL,
    price numeric NOT NULL,
    id_detalle bigint NOT NULL,
    CONSTRAINT orddetail_pkey PRIMARY KEY (id_detalle),
    CONSTRAINT ordfkey FOREIGN KEY (order_no)
        REFERENCES public.ordhead (order_no) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.orddetail
    OWNER to postgres;

--------------------------------------------------------------------------------
--SEQUENCES

-- DROP SEQUENCE IF EXISTS public.seq_producto;

CREATE SEQUENCE IF NOT EXISTS public.seq_producto
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY product.pdt_id;

ALTER SEQUENCE public.seq_producto
    OWNER TO postgres;
	

-- DROP SEQUENCE IF EXISTS public.seq_ordhead;

CREATE SEQUENCE IF NOT EXISTS public.seq_ordhead
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY ordhead.order_no;

ALTER SEQUENCE public.seq_ordhead
    OWNER TO postgres;
	
CREATE SEQUENCE IF NOT EXISTS public.seq_orddetail
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY orddetail.id_detalle;

ALTER SEQUENCE public.seq_orddetail
    OWNER TO postgres;
	

--------------------------------------------------------------------------------
--alter

alter table product alter COLUMN pdt_id set DEFAULT nextval('seq_producto'::regclass);
alter table ordhead alter COLUMN order_no set DEFAULT nextval('seq_ordhead'::regclass);
alter table orddetail alter COLUMN id_detalle set DEFAULT nextval('seq_orddetail'::regclass);

--------------------------------------------------------------------------------
--INSERTS

INSERT INTO public.users(
	usr_id, usr_name, usr_last_name, usr_nick_name, usr_password, usr_create, usr_last_update, usr_state)
	VALUES (1, 'marlon', 'campaña', 'mcampana', '1234', '2022/10/11', NULL, 'A');
	
INSERT INTO public.product(
	pdt_name, pdt_price, pdt_create, pdt_last_update, pdt_state)
	VALUES ('tabla tipo 1', 5, '12/10/2022', NULL, 'A');
	
INSERT INTO public.product(
	pdt_name, pdt_price, pdt_create, pdt_last_update, pdt_state)
	VALUES ('tabla tipo 2', 10.50, '12/10/2022', NULL, 'A');
	
	
INSERT INTO public.product(
	pdt_name, pdt_price, pdt_create, pdt_last_update, pdt_state)
	VALUES ('tabla tipo 3', 13.50, '12/10/2022', NULL, 'A');



